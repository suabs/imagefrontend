angular.module('myApp',[]);

function myController($scope, $http){
	$scope.searchData = {};
	$scope.getImagesOnClick = function(){

		$http.get('https://imgurbacked.herokuapp.com/api/imgur')
		.success(function(datas){
			$scope.images = datas;
			console.log();
		})
		.error(function(datas){
			console.log(datas);
		});
	}
	
	$scope.searchImages =function(query){

		$http.get('https://imgurbacked.herokuapp.com/api/imgur/' + $scope.searchData.query)
		.success(function(datas){
			$scope.searchData={};
			$scope.images = datas;			
		})
		.error(function(datas){
			console.log('Error: ', datas);
		});
	}

};
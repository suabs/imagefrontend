var gzippo = require('gzippo');//it compresses the static file for better performance
var express = require('express');
var app = express();

app.use(gzippo.staticGzip("" + __dirname + "/dist"));
app.listen(process.env.PORT || 5000);
Imgur FrontEnd 
======================================================
This application will connect to imgurBackend. It retrieve viral images from imgur, lets to search according to user input.

### Requirements 
* npm and Nodejs

### To run this Application
```
$ npm install 
$ npm install -g nodemon
$ nodemon web.js

```
### Deploy to Heroku
```
$ heroku git:remote -a imgur imagefrontend
$ git push heroku master

```
### [Heroku Demo url](https://imagefrontend.herokuapp.com/) 

### Source
 * [http://awaxman11.github.io/blog/2014/07/13/how-to-create-an-angular-app-using-yeoman-and-deploy-it-to-heroku/](http://awaxman11.github.io/blog/2014/07/13/how-to-create-an-angular-app-using-yeoman-and-deploy-it-to-heroku/)
 * [https://scotch.io/tutorials/creating-a-single-page-todo-app-with-node-and-angular](https://scotch.io/tutorials/creating-a-single-page-todo-app-with-node-and-angular)